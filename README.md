# COMP344 - Assignment 1 - Laptop Shop

##Student Information
**Full name**: Foxwell Tom

**Student ID**: 43690688

The **browser** I used for this assignment is: Apple Safari and Google Chrome

##Links to my online store site
1. **Site registration page @ platypus.science.mq.edu.au site**: https://platypus.science.mq.edu.au/~43690688/registration.php

2. **Site log in page @ platypus.science.mq.edu.au site**: https://platypus.science.mq.edu.au/~43690688/login.php

3. **Site logout page @ platypus.science.mq.edu.au site**: https://platypus.science.mq.edu.au/~43690688/logout.php