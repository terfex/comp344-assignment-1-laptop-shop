<?php 
// example from w3schools.com
function spamcheck($field)
{
  //filter_var() sanitizes the e-mail
  //address using FILTER_SANITIZE_EMAIL
  $field = filter_var($field, FILTER_SANITIZE_EMAIL);

  //filter_var() validates the e-mail
  //address using FILTER_VALIDATE_EMAIL
  if (filter_var($field, FILTER_VALIDATE_EMAIL))
    return TRUE;
  else
    return FALSE;
}
$from = $_POST["from"];
$to = $_POST["to"];
$subject = $_POST["subject"];
$message = $_POST["content"];
$headers = "From: " . $from;

$mailcheck = spamcheck($to);
if ($mailcheck == FALSE)
  echo "Invalid receiver's email address.";
else if (spamcheck($from) == FALSE)
  echo "Invalid sender's email address.";
else
{
  mail($to, $subject, $message, $headers);
  echo "<p>The email has been sent to</p>";
  echo "<p>$to</p>";
}
?>