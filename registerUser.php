<?php 
// example from w3schools.com
function spamcheck($field)
{
  //filter_var() sanitizes the e-mail
  //address using FILTER_SANITIZE_EMAIL
  $field = filter_var($field, FILTER_SANITIZE_EMAIL);

  //filter_var() validates the e-mail
  //address using FILTER_VALIDATE_EMAIL
  if (filter_var($field, FILTER_VALIDATE_EMAIL))
    return TRUE;
  else
    return FALSE;
}
function postcodecheck($field)
{
  // check whether postcode is 4 digits in length
  $strlength = strlen((string)$field);

  // Returns whether valid postcode entered is True or False
  if ($strlength == 4) {
    return TRUE;
  } else {
    return FALSE;
  }
}
function creditcardnumbercheck($field)
{
  // check whether postcode is 8 digits in length
  $strlength = strlen((string)$field);

  // Returns whether valid postcode entered is True or False
  if ($strlength == 8) {
    return TRUE;
  } else {
    return FALSE;
  }
}
function creditcardexpirycheck($field)
{
  // Check whether credit card date is before today
  if (strtotime($field . " 00:00:00") > strtotime(date("Y/m/d 00:00:00")))
    return TRUE;
  else
    return FALSE;
}

include("header.php");
include("dbconnect.php"); 

$user_id=md5(uniqid(rand(), true));
$firstname=$_POST["firstname"];
$lastname=$_POST["lastname"];
$streetnumber=$_POST["streetnumber"];
$streetname=$_POST["streetname"];
$suburb=$_POST["suburb"];
$state=$_POST["state"];
$postcode=$_POST["postcode"];
$phone=$_POST["phone"];
$paymenttype=$_POST["paymenttype"];
$creditcardname=$_POST["creditcardname"];
$creditcardnumber=$_POST["creditcardnumber"];
$creditcardexpiry=$_POST["creditcardexpiry"];
$cvc=$_POST["cvc"];
$username=$_POST["username"];
$password=$_POST["password"];

// 
// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: Tom Foxwell (43690688) <thomas.foxwell@students.mq.edu.au>' . "\r\n";
$headers .= 'Cc: Tom Foxwell (43690688) <thomas.foxwell@students.mq.edu.au>' . "\r\n";

$body="
<html>
  <head>
    <title>MQ Laptop Shop User Registration Confirmation</title>
  </head>
  <body>
    <h1>MQ Laptop Shop: Confirm User Registration</h1>
    <p>Thanks for registering for an account at the MQ Laptop Shop. Below are the details we have recorded your user account at time of registration.</p>
    <table>
      <tr>
        <td>First Name:</td><td>$firstname</td>
      </tr>
      <tr>
        <td>Last Name:</td><td>$lastname</td>
      </tr>
      <tr>
        <td>Username:</td><td>$username</td>
      </tr>
    </table>
    <p>Email sent from COMP344 Assignment 1 2018 by Tom Foxwell (43690688)</p>
  </body>
</html>
";
  
$sql = "INSERT INTO user (`username`, `first_name`, `last_name`, PASSWORD) VALUES ('$username', '$firstname', '$lastname', '$password');"; 
$sql .= "INSERT INTO `address` (`street_number`, `street_name`, `suburb`, `state`, `postcode`, `phone`) VALUES ('$streetnumber', '$streetname', '$suburb', '$state', '$postcode', '$phone');";
$sql .= "INSERT INTO usr_payment (`payment_type`, `creditcard_name`, `creditcard_number`, `creditcard_expiry`, `cvc`) VALUES ('$paymenttype', '$creditcardname', '$creditcardnumber', '$creditcardexpiry', '$cvc');";

$mailcheck = spamcheck($username);
$validpostcode = postcodecheck($postcode);
$validcreditcardnumber = creditcardnumbercheck($creditcardnumber);
$validcreditcardexpiry = creditcardexpirycheck($creditcardexpiry);
if ($mailcheck == FALSE)
  echo "Invalid email address.";
else if (spamcheck($username) == FALSE)
  echo "Invalid email address.";
else if ($validpostcode == FALSE)
  echo "Invalid postcode.";
else if (postcodecheck($postcode) == FALSE)
  echo "Invalid postcode.";
else if ($validcreditcardnumber == FALSE)
  echo "Invalid creditcard number.";
else if (creditcardnumbercheck($creditcardnumber) == FALSE)
  echo "Invalid creditcard number.";
else if ($validcreditcardexpiry == FALSE)
  echo "Creditcard expiry date is not after today";
else if (creditcardexpirycheck($creditcardexpiry) == FALSE)
  echo "Creditcard expiry date is not after today";
else
{
  if ($conn->multi_query($sql) === TRUE) {
    echo "Thank you for registration";
    mail($username,'MQ Laptop Shop User Registration Confirmation', $body, $headers,'-f thomas.foxwell@students.mq.edu.au');
    echo "<p>Please check your inbox for an email been sent to $username</p>";
  } else {
    echo "Sorry an error occured";
  }
}

include("dbdisconnect.php"); 

?>

