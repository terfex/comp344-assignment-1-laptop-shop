<html>
<head><title>Register</title></head>
<body>

<?php include 'header.php';?>

<form method="post"  action="registerUser.php">
<table>
<tr>
<td align="right">First name:</td>
<td align="left"><input type="text" name="firstname" /></td>
</tr>

<tr>
<td align="right">Last name:</td>
<td align="left"><input type="text" name="lastname" /></td>
</tr>

<tr>
<td align="right">Email (this will be your username):</td>
<td align="left"><input type="text" name="username" /></td>
</tr>

</table>

<h2>Shipping Address</h2>

<table>

<tr>
<td align="right">Street Number:</td>
<td align="left"><input type="text" name="streetnumber" /></td>
</tr>

<tr>
<td align="right">Street Name:</td>
<td align="left"><input type="text" name="streetname" /></td>
</tr>

<tr>
<td align="right">Suburb:</td>
<td align="left"><input type="text" name="suburb" /></td>
</tr>

<tr>
<td align="right">State:</td>
<td align="left"><input type="text" name="state" /></td>
</tr>

<tr>
<td align="right">Postcode:</td>
<td align="left"><input type="text" name="postcode" /></td>
</tr>

<tr>
<td align="right">Phone:</td>
<td align="left"><input type="text" name="phone" /></td>
</tr>

</table>

<h2>Payment Details</h2>

<table>

<tr>
<td align="right">Payment Type:</td>
<td align="left"><input type="radio" name="paymenttype" value="creditcard" checked> Credit Card</td>
</tr>

<tr>
<td align="right">Name:</td>
<td align="left"><input type="text" name="creditcardname" /></td>
</tr>

<tr>
<td align="right">Credit Card Number:</td>
<td align="left"><input type="text" name="creditcardnumber" /></td>
</tr>

<tr>
<td align="right">Expiry:</td>
<td align="left"><input type="date" name="creditcardexpiry" /></td>
</tr>

<tr>
<td align="right">CVC:</td>
<td align="left"><input type="text" name="cvc" /></td>
</tr>

</table>

<h2>Password</h2>

<table>

<tr>
<td align="right">Password:</td>
<td align="left"><input type="password" name="password" /></td>
</tr>

</table>
<input type="reset" name="Reset"/><input type="submit" name="Submit"/>
</form>
</body>
</html>
